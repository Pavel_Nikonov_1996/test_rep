<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',function (){return view('welcome');})->name('welcome');

Route::get('/test',[\App\Http\Controllers\TestController::class,'test'])->name('test');//для тестов ajax

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

route::get('/logout',[\App\Http\Controllers\LogoutController::class,'logout'])->name('logout');


route::get('/{desk_id}',[\App\Http\Controllers\DeskController::class,'show'])->name('enter_desk');//вход на доску
route::get('/{desk_id}/{thread_id}',[\App\Http\Controllers\ThreadController::class,'index'])->name('enter_thread');
