<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Desk;

class DeskController extends Controller
{
    public function show(Request $request,Desk $desks){//отображает список тредов
        $id_param = explode('_',$request->url());
        $desk_id = array_pop($id_param);
        $desk = $desks->all()->find($desk_id);
        $threads = $desk -> threads;
        $desk_name=$desk->name;
        $data = [
            'threads'=>$threads,
            'desk_name'=>$desk_name,
            'desk_id'=>$desk_id
        ];
        return view('desk')->with($data);
    }

}
