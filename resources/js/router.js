import VueRouter from "vue-router";
import Login from "./components/Login";
import Register from "./components/Register";

export default new VueRouter({routes:[
    {
        path:'/login',
        component:Login
    },
    /*{
        path:'/login/sign',
        component:Login
    },*/
    {
        path:'/register',
        component:Register
    }
    ],
    mode: 'history'
})
