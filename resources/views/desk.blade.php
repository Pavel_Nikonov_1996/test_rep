@extends('layouts.app')
@section('title')
    {{$desk_name}}
@endsection
@section('content')
    <div class="items album py-5">
    <div class="container">
    <div class="row">

        @foreach($threads as $thread)
                <a class="items-row rounded" href="{{route('enter_thread',['desk_id'=>"desk_$desk_id",'thread_id'=>"thread_$thread->id"])}}">
                    <div class="d-flex">
                        <div class="p-2 w-100 tc"><p>{{$thread->name}}</p></div>
                        @if(count($thread->comments)%10==1)
                            <div class="p-2 flex-shrink-0 tc">({{count($thread->comments)}} ответ)</div>
                        @elseif(count($thread->comments)==0)
                            <div class="p-2 flex-shrink-0 tc">(нет ответов)</div>
                        @elseif (count($thread->comments)%10<5)
                            <div class="p-2 flex-shrink-0 tc">({{count($thread->comments)}} ответа)</div>
                        @else
                            <div class="p-2 flex-shrink-0 tc">({{count($thread->comments)}} ответов)</div>
                        @endif
                    </div>
                    <div class="post-thumbnail" align="center"><img class="post-image" src="{{asset($thread->file)}}"></div>
                    <div class="items-text p-3">
                         {{$thread->message}}
                    </div>
                </a>
        @endforeach

    </div>
    </div>
    </div>
@endsection
